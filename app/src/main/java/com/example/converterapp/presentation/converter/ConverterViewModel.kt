package com.example.converterapp.presentation.converter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.converterapp.data.repository.ConverterRepositoryImpl
import com.example.converterapp.data.utils.BASE_CURRENCY
import com.example.converterapp.domain.usecase.ConvertCurrencyUseCase
import com.example.converterapp.presentation.model.CommonState
import kotlinx.coroutines.launch

class ConverterViewModel : ViewModel() {

    private val _state: MutableLiveData<CommonState> =  MutableLiveData(CommonState.ShowLoading)
    val state: LiveData<CommonState> get() = _state

    private val repository = ConverterRepositoryImpl()
    private val useCase = ConvertCurrencyUseCase(repository)

    private val _selectedCurrency = MutableLiveData(BASE_CURRENCY)
    private var conversionRate: Double? = null
    fun setConvertedToCurrency(currency: String) {
        _selectedCurrency.value = currency
        fetchConversionRate()
    }

    private fun fetchConversionRate() {
        val selectedCurrency = _selectedCurrency.value

        if (!selectedCurrency.isNullOrEmpty()) {
            viewModelScope.launch {
                try {
                    val rate = useCase(selectedCurrency)
                    if (rate != null) {
                        conversionRate = rate
                    } else {
                        _state.value = CommonState.Error("Currency conversion failed.")
                    }
                } catch (e: Exception) {
                    _state.value = CommonState.Error(e.message?:"Exception")
                }
            }
        }
    }

    fun convertValue(amountStr: String?) {
        if (!amountStr.isNullOrEmpty()) {
            val amount = amountStr.toFloat()
            val conversion = amount * (conversionRate ?: 0.0)
            _state.value = CommonState.Result(conversion.toString())
        } else {
            _state.value = CommonState.Error("Please enter an amount")
        }
    }
}
