package com.example.converterapp.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.converterapp.R
import com.example.converterapp.databinding.CurrencyItemBinding
import com.example.converterapp.domain.model.Currency

class CurrencyAdapter : RecyclerView.Adapter<CurrencyViewHolder>() {
    private var currencyList: List<Currency> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = CurrencyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CurrencyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val currency = currencyList[position]
        val currencyHolder = holder.binding
        currencyHolder.textViewName.text = currency.name
        currencyHolder.textViewChanges.text = holder.binding.root.context.getString(
            R.string.changes_24h,
            currency.rateChange
        )
        currencyHolder.textViewCurrentRate.text = holder.binding.root.context.getString(
            R.string.current_rate,
            currency.currentRate.toString()
        )
        currencyHolder.root.setBackgroundColor(currency.color)
    }

    override fun getItemCount() = currencyList.size

    @SuppressLint("NotifyDataSetChanged")
    fun updateData(list: List<Currency>){
        currencyList = list
        notifyDataSetChanged()
    }
}

class CurrencyViewHolder(val binding: CurrencyItemBinding) : ViewHolder(binding.root)