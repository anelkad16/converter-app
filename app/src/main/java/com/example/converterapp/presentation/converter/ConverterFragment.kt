package com.example.converterapp.presentation.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.converterapp.R
import com.example.converterapp.data.utils.BASE_CURRENCY
import com.example.converterapp.databinding.FragmentConverterBinding
import com.example.converterapp.presentation.model.CommonState

class ConverterFragment : Fragment() {
    private var _binding: FragmentConverterBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ConverterViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConverterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews()
        setupObserver()
    }

    private fun bindViews() {
        binding.apply {
            btnConvert.setOnClickListener {
                onConvertButtonClicked()
            }
            tvFromCurrency.text = BASE_CURRENCY
            val currencyCodes = resources.getStringArray(R.array.currency_codes)
            spToCurrency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val selectedCurrency = currencyCodes[position]
                    viewModel.setConvertedToCurrency(selectedCurrency)
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        }
    }

    private fun setupObserver() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when(state) {
                is CommonState.Error -> {
                    Toast.makeText(requireContext(), state.error, Toast.LENGTH_SHORT).show()
                }
                is CommonState.Result<*> -> {
                    binding.tvResult.text = state.result.toString()
                }
                else -> Unit
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onConvertButtonClicked() {
        viewModel.convertValue(binding.etFrom.text.toString())
    }
}
