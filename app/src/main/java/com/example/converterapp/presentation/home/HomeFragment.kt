package com.example.converterapp.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.converterapp.databinding.FragmentMainBinding
import com.example.converterapp.domain.model.Currency
import com.example.converterapp.presentation.adapter.CurrencyAdapter
import com.example.converterapp.presentation.model.CommonState

class HomeFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var currencyAdapter: CurrencyAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)

        setupRecyclerView()
        fetchData()
        setupObserver()

        return binding.root
    }

    private fun setupRecyclerView() {
        currencyAdapter = CurrencyAdapter()
        binding.recyclerView.adapter = currencyAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())

        val dividerItemDecoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        binding.recyclerView.addItemDecoration(dividerItemDecoration)
    }

    private fun fetchData() {
        viewModel.getCurrencies()
    }

    private fun setupObserver() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when(state) {
                CommonState.ShowLoading -> {
                    binding.progressBarHome.isVisible = true
                }
                CommonState.HideLoading -> {
                    binding.progressBarHome.isVisible = false
                }
                is CommonState.Error -> {
                    binding.tvErrorMessage.isVisible = true
                    binding.tvErrorMessage.text = state.error
                }
                is CommonState.Result<*> -> {
                    currencyAdapter.updateData(state.result as List<Currency>)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
