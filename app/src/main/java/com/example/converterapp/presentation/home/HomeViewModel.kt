package com.example.converterapp.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.converterapp.data.utils.BASE_CURRENCY
import com.example.converterapp.domain.usecase.CurrenciesInteractor
import com.example.converterapp.domain.usecase.GetCurrenciesUseCase
import com.example.converterapp.presentation.model.CommonState
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val _state: MutableLiveData<CommonState> = MutableLiveData(CommonState.ShowLoading)
    val state: LiveData<CommonState> get() = _state
    private val useCase = GetCurrenciesUseCase()
    private val interactor = CurrenciesInteractor(useCase)

    fun getCurrencies() {
        viewModelScope.launch {
            _state.value = CommonState.ShowLoading
            val currencyList = interactor.getCurrencies(BASE_CURRENCY)
            currencyList?.let { list ->
                _state.value = CommonState.Result(list)
            }
            _state.value = CommonState.HideLoading
        }
    }
}
