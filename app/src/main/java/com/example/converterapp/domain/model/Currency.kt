package com.example.converterapp.domain.model

data class Currency(
    val name: String,
    val currentRate: Double,
    var rateChange: Double,
    var color: Int
)
