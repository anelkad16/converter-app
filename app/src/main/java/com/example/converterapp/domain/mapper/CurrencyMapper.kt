package com.example.converterapp.domain.mapper

import android.graphics.Color
import com.example.converterapp.domain.model.Currency
import com.example.converterapp.data.model.Rates

class CurrencyMapper {
    fun mapRatesToCurrency(rates: Rates?): List<Currency> {
        return rates?.javaClass?.declaredFields?.map { field ->
            field.isAccessible = true
            val currencyCode = field.name
            val rate = field.get(rates) as? Double ?: 0.0
            Currency(
                name = currencyCode,
                currentRate = rate,
                rateChange = 0.0,
                color = Color.YELLOW
            )
        } ?: emptyList()
    }
}