package com.example.converterapp.domain.repository

interface ConverterRepository {
    suspend fun convertCurrency(selectedCurrency: String): Double?
}