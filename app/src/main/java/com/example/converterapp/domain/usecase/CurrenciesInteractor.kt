package com.example.converterapp.domain.usecase


class CurrenciesInteractor(
    private val getCurrenciesUseCase: GetCurrenciesUseCase
) {
    suspend fun getCurrencies(baseCurrency: String) =
        getCurrenciesUseCase.invoke(baseCurrency)
}