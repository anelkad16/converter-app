package com.example.converterapp.domain.usecase

import com.example.converterapp.data.repository.CurrencyRepository
import com.example.converterapp.domain.model.Currency

class GetCurrenciesUseCase(
//    private val repository: CurrencyRepositoryI
) {

    private val repository = CurrencyRepository()

    suspend operator fun invoke(baseCurrency: String): List<Currency>? =
        repository.getCurrencies(baseCurrency)
}
