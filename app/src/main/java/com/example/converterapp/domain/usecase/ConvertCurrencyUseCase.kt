package com.example.converterapp.domain.usecase

import com.example.converterapp.domain.repository.ConverterRepository

class ConvertCurrencyUseCase(
    private val repository: ConverterRepository
) {

//    private val repository = ConverterRepository()

    suspend operator fun invoke(selectedCurrency: String) =
        repository.convertCurrency(selectedCurrency)
}
