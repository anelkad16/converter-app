package com.example.converterapp.data.repository

import android.graphics.Color
import com.example.converterapp.data.remote.ApiClient
import com.example.converterapp.data.remote.ApiService
import com.example.converterapp.domain.mapper.CurrencyMapper
import com.example.converterapp.domain.model.Currency
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

class CurrencyRepository {
    private val apiService: ApiService = ApiClient.provideApi()

    private val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    private val currencyMapper = CurrencyMapper()
    private val currentDate = dateFormat.format(Date())
    private val calendar = Calendar.getInstance()

    init {
        calendar.time = Date()
        calendar.add(Calendar.DAY_OF_YEAR, -1)
    }
    suspend fun getCurrencies(baseCurrency: String): List<Currency>? = withContext(Dispatchers.IO) {
        val previousDate = dateFormat.format(calendar.time)

        val currentRatesResponse = apiService.getCurrenciesWithDate(currentDate, baseCurrency = baseCurrency)
        if (currentRatesResponse.isSuccessful) {
            val currentRates = currentRatesResponse.body()?.rates

            val previousRatesResponse =
                apiService.getCurrenciesWithDate(previousDate, baseCurrency = baseCurrency)
            val previousRates = previousRatesResponse.body()?.rates

            if (currentRates != null && previousRates != null) {
                val currencyList = currencyMapper.mapRatesToCurrency(currentRates)
                val previousList = currencyMapper.mapRatesToCurrency(previousRates)
                currencyList.forEach { currency ->
                    val previousRate =
                        previousList.find { it.name == currency.name }?.currentRate ?: 0.0
                    val rateChange = currency.currentRate - previousRate
                    currency.rateChange = rateChange

                    currency.color = if (rateChange >= 0) Color.GREEN
                    else Color.RED
                }
                return@withContext currencyList
            }
        }
        null
    }
}
