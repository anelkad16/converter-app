package com.example.converterapp.data.repository

import android.util.Log
import com.example.converterapp.data.model.ConverterResponse
import com.example.converterapp.data.model.Rates
import com.example.converterapp.data.remote.ApiClient
import com.example.converterapp.data.utils.API_KEY
import com.example.converterapp.data.utils.BASE_CURRENCY
import com.example.converterapp.domain.repository.ConverterRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ConverterRepositoryImpl: ConverterRepository {

    private val apiService = ApiClient.provideApi()

    override suspend fun convertCurrency(selectedCurrency: String): Double? = withContext(Dispatchers.IO) {
        val formattedCurrency = selectedCurrency.uppercase()
         try {
            val response = apiService.getCurrencyRates(API_KEY, BASE_CURRENCY, formattedCurrency)
            if (response.isSuccessful) {
                val data: ConverterResponse? = response.body()
                val rates = data?.rates
                val rate: Double? = rates?.let { rate ->
                    getRateByCurrencyCode(rate, formattedCurrency)
                }
                rate ?: 0.0
            } else {
                null
            }
        } catch (e: Exception) {
            Log.e("convertCurrency", "Error: ${e.message}", e)
            null
        }
    }

    private fun getRateByCurrencyCode(rates: Rates, currencyCode: String): Double? {
        val field = rates.javaClass.getDeclaredField(currencyCode)
        field.isAccessible = true
        return field.get(rates) as? Double
    }
}
