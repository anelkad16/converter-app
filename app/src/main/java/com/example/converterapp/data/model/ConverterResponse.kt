package com.example.converterapp.data.model

import com.google.gson.annotations.SerializedName

data class ConverterResponse(
    @SerializedName("base")
    val base: String,
    @SerializedName("rates")
    val rates: Rates?
)
